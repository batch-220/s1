# ACTIVITY D1S1

name = "Charles Patrick Lilagan"
age = 23
occupation = "Full-Stack Web Developer"
movie = "Joker"
rating = 99.99

print(f"I am {name} and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%.")


num1, num2, num3 = 100, 200, 300

# Product of num1 and num2
print(num1 * num2)

# num1 > num3
print(num1 > num3)

# add num3 to num2
print(num3 + num2)